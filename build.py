#!/bin/env python3
import logging, sys, os
import yaml
import pprint
import json
from jsonschema import validate

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

class parameters:
   def __init__(self, params):
      self.schema = self.load_yaml_file("base/parameters.yml")
      self.parameters = params
      logging.debug("validate data")
      self.validate_data()
      logging.debug("parameters initialized")
   # Init class via yaml or json
   @classmethod
   def from_yaml(cls, param_data):
      try:
         logging.debug("from_yaml")
         logging.debug("param_data: %s" % param_data)
         parameters1 = yaml.safe_load(param_data)
         return cls(parameters1)
      except yaml.YAMLError as exc:
         if hasattr(exc, 'problem_mark'):
             mark = exc.problem_mark
             print("Error position: (%s:%s)" % (mark.line+1, mark.column+1))
         raise Exception('This is not a yaml')
   @classmethod
   def from_json(cls, param_data):
      try:
         logging.debug("from_json")
         logging.debug("param_data: {}".format(param_data))
         parameters1 = json.loads(param_data)
         return cls(parameters1)
      except ValueError:
         raise Exception('This is not a json')
   def load_yaml_file(self, file):
      with open(file, 'r') as stream:
         return(yaml.safe_load(stream))
   def validate_data(self):
      logging.debug("enumerate_data: {}".format(str(t for t in enumerate(self.parameters))))
      for idx, item in enumerate(self.parameters):
         logging.debug("validate_data:idx: {}".format(idx))
         logging.debug("validate_data:item: {}".format(item))
         try:
            validate(item, self.schema)
         except jsonschema.exceptions.ValidationError as ve:
            logging.exception("error validating data on: idx {}: ERROR: {}".format(idx).format(str(ve)))

try:
   data = os.environ['parameters']
except Exception as key:
   #TODO from files
   logging.exception("%s variable not defined or error" % key)
   raise SystemExit(-1)

logging.debug("input parameters: %s" % data)
try:
   user = parameters.from_yaml(data)
except Exception as err:
   logging.warning('%s\nTrying for a json' % err)
   user = parameters.from_json(data)

logging.debug("parameters: \n%s" % user.parameters)
logging.debug("schema: \n%s" % user.schema)
