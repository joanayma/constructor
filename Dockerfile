FROM alpine:3.7
LABEL name="registry.gitlab.com/joanayma/alpine-constructor"

RUN apk --no-cache install python3 py3-yaml py3-jsonschema
COPY base/. /root/base/.
COPY entrypoint.py build.py /root/.

ONBUILD USER_PARAM
ONBUILD ENV USER_PARAM=$USER_PARAM
ONBUILD RUN /root/build.py

ENTRYPOINT /root/entrypoint.py
